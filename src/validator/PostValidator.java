package validator;

import static utils.ValidationUtil.*;

import beans.Post;

/**
 * 投稿バリデーター
 */
public class PostValidator {

	/**
	  * 件名
	  * @param post
	  * @return
	  */
	public static String isValidTitle(Post post) {

		if ( isValidBlank(post.getTitle()) ) {
			return "入力必須項目です";
		}
		if ( !(isValidRange(1, 30, post.getTitle())) ) {
			return "件名は30文字以下で入力してください";
		}

		return null;
	}

	/**
	  * カテゴリー
	  * @param post
	  * @return
	  */
	public static String isValidCategory(Post post) {

		if ( isValidBlank(post.getCategory()) ) {
			return "入力必須項目です";
		}
		if ( !(isValidRange(1, 10, post.getCategory())) ) {
			return "カテゴリーは10文字以下で入力してください";
		}

		return null;
	}

	/**
	  * 本文
	  * @param post
	  * @return
	  */
	public static String isValidText(Post post) {

		if ( isValidBlank(post.getText()) ) {
			return "入力必須項目です";
		}
		if ( !(isValidRange(1, 1000, post.getText())) ) {
			return "本文は1000文字以下で入力してください";
		}

		return null;
	}
}
