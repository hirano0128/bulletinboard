package validator;

import static utils.ValidationUtil.*;

import beans.User;
import service.UserService;
import utils.CipherUtil;

/**
 * ユーザーバリデーター
 */
public final class UserValidator {

	private static final int EMPTY = 0;
	private static final int AUTHORITY_USER = 1;		// 権限ユーザー
	private static final int INFORMATION_SECURITY = 2;	// 情報セキュリティ部
	private static final int STORE_MANAGER = 3;			// 店長

	/**
	  * ログインID(新規登録)
	  * @param loginId
	  * @param message
	  * @return
	  */
	public static String isValidLoginId(User user) {

		if ( isValidBlank(user.getLoginId()) ) {
			return "入力必須項目です";
		}
		if ( new UserService().isDuplicationUser(user.getLoginId()) ) {
			return "そのログインIDは既に使用されています";
		}
		if ( !(isValidFormat("^[a-z A-Z 0-9]+$", user.getLoginId())) ) {
			return "ログインIDは半角英数字で入力してください";
		}
		if ( !(isValidRange(6, 20, user.getLoginId())) ) {
			return "ログインIDは6文字以上20文字以下で入力してください";
		}

		return null;
	}

	/**
	  * ログインID(編集)
	  * @param user
	  * @param editUser
	  * @param message
	  * @return
	  */
	public static String isValidLoginId(User user, User editUser) {

		if ( isValidEmpty(user.getLoginId()) ) {
			return "入力必須項目です";
		}
		if ( !(isValidEquals(user.getLoginId(), editUser.getLoginId())) &&
				new UserService().isDuplicationUser(user.getId(), user.getLoginId()) ) {
			return "そのログインIDは既に使用されています";
		}
		if ( !(isValidFormat("^[a-z A-Z 0-9]+$", user.getLoginId())) ) {
			return "ログインIDは半角英数字で入力してください";
		}
		if ( !(isValidRange(6, 20, user.getLoginId())) ) {
			return "ログインIDは6文字以上20文字以下で入力してください";
		}

		return null;
	}

	/**
	  * パスワード(新規登録)
	  * @param password
	  * @param confirmation
	  * @param message
	  * @return
	  */
	public static String isValidPassword(User user) {

		if ( isValidEmpty(user.getPassword()) ) {
			return "入力必須項目です";
		}
		if ( !(isValidFormat("^[ -~]+$", user.getPassword())) ) {
			return "パスワードは半角文字(記号含む)で入力してください";
		}
		if ( !(isValidRange(6, 20, user.getPassword())) ) {
			return "パスワードは6文字以上20文字以下で入力してください";
		}

		return null;
	}

	/**
	  * パスワード(編集)
	  * @param user
	  * @param confirmation
	  * @param message
	  * @return
	  */
	public static String isValidEditPassword(User user) {

		if ( isValidEmpty(user.getPassword()) ) {
			user.setPassword(null);
			return null;
		}
		if ( !isValidFormat("^[ -~]+$", user.getPassword()) ) {
			return "パスワードは半角文字(記号含む)で入力してください";
		}
		if ( !(isValidRange(6, 20, user.getPassword())) ) {
			return "パスワードは6文字以上20文字以下で入力してください";
		}

		return null;
	}

	/**
	  * パスワード(変更)
	  * @param password
	  * @param confirmation
	  * @param message
	  * @return
	  */
	public static String isValidUpdatePassword(User loginUser, String password) {

		if ( isValidEmpty(password) ) {
			return "入力必須項目です";
		}
		if ( !(isValidFormat("^[ -~]+$", password)) ) {
			return "パスワードは半角文字(記号含む)で入力してください";
		}
		if ( !(isValidRange(6, 20, password)) ) {
			return "パスワードは6文字以上20文字以下で入力してください";
		}
		if ( isValidEquals(CipherUtil.encrypt(password), loginUser.getPassword()) ) {
			return "変更前のパスワードは使用できません";
		}

		return null;
	}

	/**
	  * パスワード再入力(新規登録)
	  * @param password
	  * @param confirmation
	  * @param message
	  * @return
	  */
	public static String isValidConfirmation(String password, String confirmation) {

		if ( isValidEmpty(password) && isValidEmpty(confirmation) ) {
			return "入力必須項目です";
		}
		if ( !(isValidEquals(password, confirmation)) ) {
			return "パスワードが一致しません";
		}

		return null;
	}

	/**
	  * パスワード再入力(編集)
	  * @param password
	  * @param confirmation
	  * @param message
	  * @return
	  */
	public static String isValidEditConfirmation(String password, String confirmation) {

		if ( isValidEmpty(password) && isValidEmpty(confirmation)) {
			return null;
		}
		if ( !(isValidEquals(password, confirmation)) ) {
			return "パスワードが一致しません";
		}

		return null;
	}

	/**
	  * 現在のパスワード
	  * @param loginUser
	  * @param oldPassword
	  * @param message
	  * @return
	  */
	public static String isValidOldPassword(User loginUser, String oldPassword) {

		if ( isValidEmpty(oldPassword) ) {
			return "入力必須項目です";
		}
		if ( !(isValidEquals(loginUser.getPassword(), CipherUtil.encrypt(oldPassword))) ) {
			return "現在のパスワードが間違っています";
		}

		return null;
	}

	/**
	  * 名称
	  * @param user
	  * @param message
	  * @return
	  */
	public static String isValidName(User user) {

		if ( isValidEmpty(user.getName()) ) {
			return "入力必須項目です";
		}
		if ( !(isValidRange(1, 10, user.getName())) ) {
			return "名称は10文字以下で入力してください";
		}

		return null;
	}

	/**
	  * 支店(新規登録)
	  * @param user
	  * @param message
	  * @return
	  */
	public static String isValidBranch(User user) {

		if ( user.getBranchId() == EMPTY ) {
			return "入力必須項目です";
		}

		return null;
	}

	/**
	  * 支店(編集)
	  * @param user
	  * @param editUser
	  * @param message
	  * @return
	  */
	public static String isValidBranch(User user, User editUser) {

		if ( editUser.getDepartmentId() == AUTHORITY_USER ) {
			return null;
		}
		if ( user.getBranchId() == EMPTY ) {
			return "入力必須項目です";
		}

		return null;
	}

	/**
	  * 部署・役職(新規登録)
	  * @param user
	  * @param message
	  * @return
	  */
	public static String isValidDepartment(User user) {

		if ( user.getDepartmentId() == EMPTY ) {
			return "入力必須項目です";
		}
		if ( user.getBranchId() == AUTHORITY_USER && user.getDepartmentId() == STORE_MANAGER ) {
			return "本社にその役職は存在しません";
		}
		if ( user.getBranchId() != AUTHORITY_USER ) {
			if (  user.getDepartmentId() == AUTHORITY_USER || user.getDepartmentId() == INFORMATION_SECURITY ) {
				return "支店にその役職は存在しません";
			}
			if ( user.getDepartmentId() == STORE_MANAGER &&
					new UserService().isDuplicationStoreManager(user.getBranchId()) ) {
				return "指定の支店には既に店長が設定されています";
			}
		}

		return null;
	}

	/**
	  * 部署・役職(編集)
	  * @param user
	  * @param editUser
	  * @param message
	  * @return
	  */
	public static String isValidDepartment(User user, User editUser) {

		if ( editUser.getDepartmentId() == AUTHORITY_USER) {
			return null;
		}
		if ( user.getDepartmentId() == EMPTY ) {
			return "入力必須項目です";
		}
		if ( user.getBranchId() == AUTHORITY_USER && user.getDepartmentId() == STORE_MANAGER ) {
			return "本社にその役職は存在しません";
		}
		if ( user.getBranchId() != AUTHORITY_USER ) {
			if (  user.getDepartmentId() == AUTHORITY_USER || user.getDepartmentId() == INFORMATION_SECURITY ) {
				return "支店にその役職は存在しません";
			}
			if ( user.getDepartmentId() == STORE_MANAGER &&
					new UserService().isDuplicationStoreManager(user.getId(), user.getBranchId()) ) {
				return "指定の支店には既に店長が設定されています";
			}
		}

		return null;
	}
}
