package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;
import service.PostService;

/**
 * 投稿削除
 */
@WebServlet(urlPatterns = { "/deletePost" })
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int postId = Integer.parseInt(request.getParameter("deletePost"));

		// 投稿の削除
		new PostService().deletePost(postId);
		// 投稿に対するコメントの削除
		new CommentService().deletePostComment(postId);

		response.sendRedirect("./");
	}

}
