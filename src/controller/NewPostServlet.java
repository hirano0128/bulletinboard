
package controller;

import static utils.ValidationUtil.*;
import static validator.PostValidator.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Post;
import beans.User;
import service.PostService;

/**
 * 新規投稿画面

 */
@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> categoryList = new PostService().getCategoryAll();

		request.setAttribute("categoryList", categoryList);

		request.getRequestDispatcher("/newpost.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// ログインユーザーの取得
		User user = (User) request.getSession().getAttribute("loginUser");

		// 投稿情報の設定
		Post post = Post.build(request.getParameterMap(), user);

		// バリデーション
		if (isValid(request, post)) {
			List<String> categoryList = new PostService().getCategoryAll();

			request.setAttribute("categoryList", categoryList);

			request.getRequestDispatcher("/newpost.jsp").forward(request, response);
			return;
		}

		// 新規投稿の登録
		new PostService().register(post);

		response.sendRedirect("./");
	}

	private boolean isValid(HttpServletRequest request, Post post) {

		Map<String, String> messages = new HashMap<String, String>();

		// 件名
		messages.put("title", isValidTitle(post));

		// カテゴリ－
		messages.put("category", isValidCategory(post));

		// 本文
		messages.put("text", isValidText(post));

		if (isNullMapValue(messages)) {
			return false;
		} else {
			// エラーメッセージをリクエストに追加
			request.getSession().setAttribute("errorMessages", messages);
			// フォームに入力したデータを保持
			request.setAttribute("post", post);

			return true;
		}
	}
}
