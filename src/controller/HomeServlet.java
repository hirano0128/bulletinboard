package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Post;
import beans.User;
import service.CommentService;
import service.PostService;
import service.UserService;

/**
 * ホーム画面(投稿・コメント表示)
 * 投稿の絞り込み検索(カテゴリー・投稿日時)
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		// 絞り込み検索(カテゴリーと投稿日時)
		try {
			// 画面に表示する情報のリストを取得
			List<User> userList = new UserService().getUserAll();
			List<Post> postList = new PostService().getPostAll();
			List<Comment> commentList = new CommentService().getCommentAll();
			List<String> categoryList = new PostService().getCategoryAll();

			request.setAttribute("categoryList", categoryList);

			// 検索フォームの入力情報を取得
			String category = request.getParameter("categories");
			String firstDate = request.getParameter("firstDate");
			String lastDate = request.getParameter("lastDate");

			// 新しい順・古い順の選択情報の取得(デフォルトは新しい順)
			int sortType = (request.getParameter("sortType") == null) ? 1
						 : Integer.parseInt(request.getParameter("sortType"));

			// 投稿がある場合のみ
			if ( !(postList.isEmpty()) ) {
				// 開始日と終了日を比較し、大小関係を調べる
				int compareDate = 0;
				if (firstDate != null && lastDate != null) {
					if (!firstDate.isEmpty() && !lastDate.isEmpty()) {
						compareDate = lastDate.compareTo(firstDate);
					}
				}

				// 開始日(入力された日付の00:00:00)
				// 未入力時 ： 最初の投稿日時を取得
				String first = (firstDate == null) ? (new PostService().getFirstDate())
						  	 : (firstDate.isEmpty()) ? (new PostService().getFirstDate())
						  	 : (compareDate < 0) ? (lastDate + " 00:00:00")
						  	 : (firstDate + " 00:00:00");

				// 終了日(入力された日付の23:59:59)
				// 未入力時 ： 最後の投稿日時を取得
				String last = (lastDate == null) ? new PostService().getLastDate()
				    	    : (lastDate.isEmpty()) ? new PostService().getLastDate()
				    	    : (compareDate < 0) ? (firstDate + " 23:59:59")
				    	    : (lastDate + " 23:59:59");

				// 投稿の検索
				postList = new PostService().searchPost(category, first, last, sortType);
			}

			// 選択可能な日付を取得
			String minDate = new PostService().getFirstDate();
			String maxDate = new PostService().getLastDate();

			// 画面を表示するための情報の設定
			request.setAttribute("minDate", minDate);
			request.setAttribute("maxDate", maxDate);
			request.setAttribute("firstDate", firstDate);
			request.setAttribute("lastDate", lastDate);

			request.setAttribute("selectedCategory", category);
			request.setAttribute("sortType", sortType);

			request.setAttribute("postList", postList);
			request.setAttribute("userList", userList);
			request.setAttribute("commentList", commentList);

			request.getRequestDispatcher("/home.jsp").forward(request, response);

		} catch (ParseException e) {
			System.out.println(e);
		}
	}
}
