package controller;

import static utils.ValidationUtil.*;
import static validator.UserValidator.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

/**
 * パスワード変更画面

 */
@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("/settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		// ログインユーザーの取得
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		// 新しいパスワードの取得
		String password = request.getParameter("password");

		// バリデーション
		if (isValid(request, loginUser, password)) {
			request.getRequestDispatcher("/settings.jsp").forward(request, response);
			return;
		}

		// パスワードの更新
		new UserService().updatePassword(loginUser, password);

		loginUser = new UserService().getUser(loginUser.getId());

		request.getSession().setAttribute("loginUser", loginUser);

		response.sendRedirect("./");
	}

	private boolean isValid(HttpServletRequest request, User loginUser, String password) {

		Map<String, String> messages = new HashMap<String, String>();
		String oldPassword = request.getParameter("oldPassword");
		String confirmation = request.getParameter("confirmation");

		// 現在のパスワード
		messages.put("oldPassword", isValidOldPassword(loginUser, oldPassword));

		// 新しいパスワード
		messages.put("password", isValidUpdatePassword(loginUser, password));

		// パスワードの再入力
		messages.put("confirmation", isValidConfirmation(password, confirmation));

		if (isNullMapValue(messages)) {
			return false;
		} else {
			// エラーメッセージをリクエストに追加
			request.getSession().setAttribute("errorMessages", messages);
			// フォームに入力したデータを保持
			request.setAttribute("oldPassword", oldPassword);
			request.setAttribute("password", password);
			request.setAttribute("confirmation", confirmation);

			return true;
		}
	}
}
