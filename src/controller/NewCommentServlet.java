package controller;

import static utils.ValidationUtil.*;
import static validator.CommentValidator.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.User;
import service.CommentService;

/**
 * コメント投稿

 */
@WebServlet(urlPatterns = { "/newComment" } )
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// ログインユーザーの取得
		User user = (User) request.getSession().getAttribute("loginUser");

		// コメント情報の設定
		Comment comment = Comment.build(request.getParameterMap(), user);

		// バリデーション
		if (isValid(request, comment)) {
			response.sendRedirect("./");
			return;
		}

		// コメントの登録
		new CommentService().register(comment);

		response.sendRedirect("./");
	}

	private boolean isValid(HttpServletRequest request, Comment comment) {

		Map<String, String> messages = new HashMap<String, String>();

		// 本文
		messages.put("commentText", isValidText(comment));

		if (isNullMapValue(messages)) {
			return false;
		} else {
			// エラーメッセージをリクエストに追加
			request.getSession().setAttribute("errorMessages", messages);
			// フォームに入力したデータを保持
			request.setAttribute("comment", comment);

			return true;
		}
	}

}
