package controller;

import static utils.ValidationUtil.*;
import static validator.UserValidator.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * ユーザー編集画面
 */
@WebServlet(urlPatterns = { "/editUser" })
public class EditUserServlet extends HttpServlet {

	private static final int EMPTY = 0;
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		// クエリ文字列チェック
		String queryString = request.getQueryString();
		// nullチェック
		if (queryString == null) {
			response.sendRedirect("adminUser");
			return;
		}

		// リクエストパラメータチェック
		String editUserId = request.getParameter("editUserId");
		// nullチェック
		if (editUserId == null) {
			response.sendRedirect("adminUser");
			return;
		}
		// 数字チェック
		if (!(editUserId.matches("^[0-9]+$"))) {
			response.sendRedirect("adminUser");
			return;
		}

		// ユーザーIDチェック
		int userId = Integer.parseInt(editUserId);
		List<User> userList = new UserService().getUserAll();
		int count = 0;
		// 存在するユーザーIDかどうか調べる
		for (User user : userList) {
			if (userId == user.getId()) {
				count++;
			}
		}
		if (count == 0) {
			response.sendRedirect("adminUser");
			return;
		}

		User user = new UserService().getUser(userId);
		user.setPassword(null);

		List<Branch> branchList = new BranchService().getBranchAll();
		List<Department> departmentList = new DepartmentService().getDepartmentAll();

		// 入力フォームに表示する情報の設定
		request.setAttribute("user", user);
		request.setAttribute("branchList", branchList);
		request.setAttribute("departmentList", departmentList);

		request.getRequestDispatcher("/edituser.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		User loginUser = (User) request.getSession().getAttribute("loginUser");

		// ユーザー情報の入力
		User user = new User();
		user.setId( Integer.parseInt(request.getParameter("userId")) );
		user.setLoginId( request.getParameter("loginId") );
		user.setPassword( request.getParameter("password") );
		user.setName( request.getParameter("name") );

		// ログインユーザー(編集を行っている権限ユーザー)チェック
		boolean isAuthorityUser = true;
		if ( loginUser.getId() != user.getId() ) {
			// ログインユーザーでない場合
			// 支店と部署・役職を編集可能にする
			Branch branch = new BranchService().getBranch( Integer.parseInt(request.getParameter("branches")) );
			Department department = new DepartmentService().getDepartment(  Integer.parseInt(request.getParameter("departments")) );

			user.setBranchId(branch.getId());
			user.setDepartmentId(department.getId());
			isAuthorityUser = false;
		} else {
			// ログインユーザーの場合
			// 支店と部署・役職を編集不可にする
			user.setBranchId(EMPTY);
			user.setDepartmentId(EMPTY);
		}

		// バリデーション
		if (isValid(request, user)) {
			// 支店と部署・役職のリストの取得
			List<Branch> branchList = new BranchService().getBranchAll();
			List<Department> departmentList = new DepartmentService().getDepartmentAll();

			request.setAttribute("branchList", branchList);
			request.setAttribute("departmentList", departmentList);

			request.getRequestDispatcher("/edituser.jsp").forward(request, response);
			return;
		}

		// ユーザー情報の更新
		if (isAuthorityUser) {
			// 管理者権限あり
			new UserService().editAuthorityUser(user);

			if (loginUser.getId() == user.getId()) {
				loginUser.setName(user.getName());
				request.setAttribute("loginUser", loginUser);
			}
		} else {
			// 権限なし
			new UserService().editUser(user);
		}

		// ユーザー管理画面へのリダイレクト
		response.sendRedirect("adminUser");
	}

	private boolean isValid(HttpServletRequest request, User user) {

		Map<String, String> messages = new HashMap<String, String>();
		String confirmation = request.getParameter("confirmation");

		int userId = Integer.parseInt(request.getParameter("userId"));
		User editUser = new UserService().getUser(userId);

		// ログインID
		messages.put("loginId", isValidLoginId(user, editUser));

		// パスワード
		messages.put("password", isValidEditPassword(user));

		// パスワードの再入力
		messages.put("confirmation", isValidEditConfirmation(user.getPassword(), confirmation));

		// 名称
		messages.put("name", isValidName(user));

		// 支店
		messages.put("branch", isValidBranch(user, editUser));

		// 部署・役職
		messages.put("department", isValidDepartment(user, editUser));

		if (isNullMapValue(messages)) {
			return false;
		} else {
			// エラーメッセージをリクエストに追加
			request.getSession().setAttribute("errorMessages", messages);
			// フォームに入力したデータを保持
			request.setAttribute("user", user);
			request.setAttribute("confirmation", confirmation);

			return true;
		}
	}
}
