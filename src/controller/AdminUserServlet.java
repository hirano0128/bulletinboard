package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


//ユーザー管理画面
@WebServlet(urlPatterns = { "/adminUser" })
public class AdminUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		// 画面を表示するための情報を取得
		List<User> userList = new UserService().getUserAll();
		List<Branch> branchList = new BranchService().getBranchAll();
		List<Department> departmentList = new DepartmentService().getDepartmentAll();

		request.setAttribute("userList", userList);
		request.setAttribute("branchList", branchList);
		request.setAttribute("departmentList", departmentList);

		request.getRequestDispatcher("/adminuser.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		int userId = Integer.parseInt(request.getParameter("stopUser"));
		User user = new UserService().getUser(userId);

		// ユーザーの停止・復活
		new UserService().isStopped(user);

		response.sendRedirect("adminUser");
	}
}
