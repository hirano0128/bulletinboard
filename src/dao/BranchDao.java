package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {
	/**
	  * 支店リストの取得
	  * @return
	  */
	public static List<Branch> getBranchAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches ORDER BY id");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	/**
	  * 支店が存在することを確かめる
	  * @param connection
	  * @param branchId
	  * @return
	  */
	public static boolean isExists(Connection connection, int branchId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, branchId);

			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = toBranchList(rs);

			if (branchList.isEmpty()) {
				return false;
			} else {
				return true;
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から支店オブジェクトのリストを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				ret.add(branch);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 支店の名前から支店を取得
	  * @param connection
	  * @param branchName
	  * @return
	  */
	public static Branch getBranch(Connection connection, String branchName) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches WHERE name = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, branchName);

			ResultSet rs = ps.executeQuery();
			Branch ret = toBranch(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 支店IDから支店を取得
	  * @param connection
	  * @param branchId
	  * @return
	  */
	public static Branch getBranch(Connection connection, int branchId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, branchId);

			ResultSet rs = ps.executeQuery();
			Branch ret = toBranch(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から支店オブジェクトを取得
	  * @param rs
	  * @return
	  */
	private static Branch toBranch(ResultSet rs) throws SQLException {

		Branch ret = new Branch();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				ret = branch;
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 支店の名前から支店IDを取得
	  * @param connection
	  * @param branchName
	  * @return
	  */
	public static int getBranchId(Connection connection, String branchName) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id FROM branches WHERE name = ?");

			ps = connection.prepareStatement(sql.toString());
			if (branchName != null) {
				ps.setString(1, branchName);
			}

			ResultSet rs = ps.executeQuery();
			int ret = toBranchId(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から支店IDを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static int toBranchId(ResultSet rs) throws SQLException {

		int ret = 0;
		try {
			while (rs.next()) {
				int id = rs.getInt("id");

				ret = id;
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 支店IDから支店の名前を取得
	  * @param connection
	  * @param branchId
	  * @return
	  */
	public static String getBranchName(Connection connection, int branchId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT name FROM branches WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, branchId);

			ResultSet rs = ps.executeQuery();
			String ret = toBranchName(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から支店の名前を取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static String toBranchName(ResultSet rs) throws SQLException {

		String ret = null;
		try {
			while (rs.next()) {
				String name = rs.getString("name");

				ret = name;
			}

			return ret;
		} finally {
			close(rs);
		}
	}

}
