package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	/**
	  * 部署・役職リストの取得
	  * @param connection
	  * @return
	  */
	public static List<Department> getDepartmentAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM departments ORDER BY id");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Department> ret = toDepartmentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 部署・役職が存在することを確かめる
	  * @param connection
	  * @param departmentId
	  * @return
	  */
	public static boolean isExists(Connection connection, int departmentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM departments WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, departmentId);

			ResultSet rs = ps.executeQuery();
			List<Department> departmentList = toDepartmentList(rs);

			if (departmentList.isEmpty()) {
				return false;
			} else {
				return true;
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から部署・役職オブジェクトのリストを取得
	  * @param rs
	  * @return
	  */
	private static List<Department> toDepartmentList(ResultSet rs) throws SQLException {

		List<Department> ret = new ArrayList<Department>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Department department = new Department();
				department.setId(id);
				department.setName(name);

				ret.add(department);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 部署・役職の名前から部署・役職を取得
	  * @param connection
	  * @param departmentName
	  * @return
	  */
	public static Department getDepartment(Connection connection, String departmentName) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM departments WHERE name = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, departmentName);

			ResultSet rs = ps.executeQuery();
			Department ret = toDepartment(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 部署・役職IDから支店を取得
	  * @param connection
	  * @param branchId
	  * @return
	  */
	public static Department getDepartment(Connection connection, int departmentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM departments WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, departmentId);

			ResultSet rs = ps.executeQuery();
			Department ret = toDepartment(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から部署・役職オブジェクトを取得
	  * @param rs
	  * @return
	  */
	private static Department toDepartment(ResultSet rs) throws SQLException {

		Department ret = new Department();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Department department = new Department();
				department.setId(id);
				department.setName(name);

				ret = department;
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 部署・役職の名前から部署・役職IDを取得
	  * @param connection
	  * @param departmentName
	  * @return
	  */
	public static int getDepartmentId(Connection connection, String departmentName) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id FROM departments WHERE name = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, departmentName);

			ResultSet rs = ps.executeQuery();
			int ret = toDepartmentId(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から部署・役職IDを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static int toDepartmentId(ResultSet rs) throws SQLException {

		int ret = 0;
		try {
			while (rs.next()) {
				int id = rs.getInt("id");

				ret = id;
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 部署・役職IDから部署・役職の名前を取得
	  * @param connection
	  * @param departmentName
	  * @return
	  */
	public static String getDepartmentName(Connection connection, int departmentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT name FROM departments WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, departmentId);

			ResultSet rs = ps.executeQuery();
			String ret = toDepartmentName(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から部署・役職の名前を取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static String toDepartmentName(ResultSet rs) throws SQLException {

		String ret = null;
		try {
			while (rs.next()) {
				String name = rs.getString("name");

				ret = name;
			}

			return ret;
		} finally {
			close(rs);
		}
	}
}
