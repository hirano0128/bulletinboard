package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import beans.Post;
import exception.SQLRuntimeException;

public class PostDao {

	//private static final int DESC = 0;
	private static final int ASC = 0;

	/**
	  * 投稿の追加
	  * @param connection
	  * @param post
	  */
	public static void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts (");
			sql.append("  id");
			sql.append(", title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_at");
			sql.append(", update_at");
			sql.append(", user_id");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(") VALUES (");
			sql.append("  NULL");	// id
			sql.append(", ?");		// title
			sql.append(", ?");		// text
			sql.append(", ?");		// category
			sql.append(", CURRENT_TIMESTAMP");	// created_at
			sql.append(", CURRENT_TIMESTAMP");	// update_at
			sql.append(", ?");		// user_id
			sql.append(", ?");		// branch_id
			sql.append(", ?");		// department_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, post.getTitle());
			ps.setString(2, post.getText());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getUserId());
			ps.setInt(5, post.getBranchId());
			ps.setInt(6, post.getDepartmentId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 投稿のリストを取得(重複なし)
	  * @param connection
	  * @return
	  */
	public static List<Post> getPostAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM posts ORDER BY id DESC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 投稿の検索
	  * @param connection
	  * @param category
	  * @param startDate
	  * @param endDate
	  * @param sortType
	  * @return
	  */
	public static List<Post> searchPost(Connection connection, String category,
			String firstDate, String lastDate, int sortType) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM posts");
			sql.append(" WHERE created_at BETWEEN ? AND ?");

			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
			if (category != null && !(category.isEmpty())) {
				// カテゴリーあり
				sql.append(" AND category = ?");
				if (sortType == ASC) {
					sql.append(" ORDER BY created_at ASC");
				} else {
					sql.append(" ORDER BY created_at DESC");
				}

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, firstDate);
				ps.setString(2, lastDate);
				ps.setString(3, category);
			} else {
				// カテゴリーなし
				if (sortType == ASC) {
					sql.append(" ORDER BY created_at ASC");
				} else {
					sql.append(" ORDER BY created_at DESC");
				}

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, firstDate);
				ps.setString(2, lastDate);
			}

			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から投稿のリストを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static List<Post> toPostList(ResultSet rs) throws SQLException {

		List<Post> ret = new ArrayList<Post>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updateAt = rs.getTimestamp("update_at");
				int userId = rs.getInt("user_id");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");

				Post post = new Post();
				post.setId(id);
				post.setTitle(title);
				post.setText(text);
				post.setCategory(category);
				post.setCreatedAt(createdAt);
				post.setUpdateAt(updateAt);
				post.setUserId(userId);
				post.setBranchId(branchId);
				post.setDepartmentId(departmentId);

				ret.add(post);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * カテゴリーのリストを取得(重複なし)
	  * @param connection
	  * @return
	  */
	public static List<String> getCategoryAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT DISTINCT category FROM posts");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<String> ret = toStringList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から文字列のリストを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static List<String> toStringList(ResultSet rs) throws SQLException {

		List<String> ret = new ArrayList<String>();
		try {
			while (rs.next()) {
				String category = rs.getString("category");

				ret.add(category);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * 投稿の削除
	  * @param connection
	  * @param postId
	  */
	public static void delete(Connection connection, int postId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM posts WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, postId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 最古の投稿日時を取得
	  * @param connection
	  * @return
	  */
	public static String getFirstDate(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT MIN(created_at) AS created_at FROM posts");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			String ret = toStringDate(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 最新の投稿日時(現在日時)を取得
	  * @param connection
	  * @return
	  */
	public static String getLastDate(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT MAX(created_at) AS created_at FROM posts");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			String ret = toStringDate(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果から日付を取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static String toStringDate(ResultSet rs) throws SQLException {

		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("yyyy-MM-dd");

		String ret = new String();
		try {
			while (rs.next()) {
				Timestamp createdAt = rs.getTimestamp("created_at");

				ret = sdf.format(createdAt);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

}
