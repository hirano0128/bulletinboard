package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	private static final int USER_ACTIVE = 1;
	private static final int USER_STOP = 0;
	private static final int STORE_MANAGER = 3;

	/**
	  * ログインユーザーの取得
	  * @param connection
	  * @param loginId
	  * @param password
	  * @return
	  */
	public static User getLoginUser(Connection connection, String loginId, String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");
			sql.append(" WHERE login_id = ?");
			sql.append(" AND password = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty()) {
				return null;
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ユーザーリストの取得
	  * @param connection
	  * @return
	  */
	public static List<User> getUserAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果からユーザーのリストを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int isStopped = rs.getInt("is_stopped");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updateAt = rs.getTimestamp("update_at");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setIsStopped(isStopped);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setCreatedAt(createdAt);
				user.setUpdateAt(updateAt);

				ret.add(user);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * IDからユーザーを取得
	  * @param connection
	  * @param userId
	  * @return
	  */
	public static User getUser(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			User ret = toUser(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ログインIDからユーザーを取得
	  * @param connection
	  * @param loginId
	  * @return
	  */
	public static User getUser(Connection connection, String loginId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE login_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			User ret = toUser(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果からユーザーを取得
	  * @param rs
	  * @return
	  */
	private static User toUser(ResultSet rs) throws SQLException {

		User ret = new User();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int isStopped = rs.getInt("is_stopped");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updateAt = rs.getTimestamp("update_at");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setIsStopped(isStopped);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setCreatedAt(createdAt);
				user.setUpdateAt(updateAt);

				ret = user;
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * ユーザーの追加(新規登録)
	  * @param connection
	  * @param user
	  */
	public static void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users (");
			sql.append("  id");
			sql.append(", login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", is_stopped");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", created_at");
			sql.append(", update_at");
			sql.append(") VALUES (");
			sql.append("  NULL");	// id
			sql.append(", ?");		// login_id
			sql.append(", ?");		// password
			sql.append(", ?");		// name
			sql.append(", ?");		// is_stopped
			sql.append(", ?");		// branch_id
			sql.append(", ?");		// department_id
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // update_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, USER_ACTIVE);
			ps.setInt(5, user.getBranchId());
			ps.setInt(6, user.getDepartmentId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ユーザーの更新(編集)
	  * @param connection
	  * @param user
	  */
	public static void updateUser(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			if ( StringUtils.isBlank(user.getPassword()) ) {
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", department_id = ?");
				sql.append(", update_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getDepartmentId());
				ps.setInt(5, user.getId());
			} else {
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", password = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", department_id = ?");
				sql.append(", update_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getDepartmentId());
				ps.setInt(6, user.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 権限のあるユーザーの更新(編集)
	  * @param connection
	  * @param user
	  */
	public static void updateAuthorityUser(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			if ( StringUtils.isBlank(user.getPassword()) ) {
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", name = ?");
				sql.append(", update_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getId());
			} else {
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", password = ?");
				sql.append(", name = ?");
				sql.append(", update_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * パスワードの更新
	  * @param connection
	  * @param loginUser
	  * @param password
	  */
	public static void updatePassword(Connection connection, User loginUser, String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" password = ?");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, password);
			ps.setInt(2, loginUser.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ユーザーの停止・復活の更新
	  * @param connection
	  * @param user
	  */
	public static void isStopped(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_stopped = ?");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			if (user.getIsStopped() == USER_STOP) {
				ps.setInt(1, USER_ACTIVE);
			}
			if (user.getIsStopped() == USER_ACTIVE) {
				ps.setInt(1, USER_STOP);
			}
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ユーザーの重複チェック(ユーザー登録時)
	  * @param connection
	  * @param loginId
	  * @return
	  */
	public static boolean isDuplicatonUser(Connection connection, String loginId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");
			sql.append(" WHERE login_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ユーザーの重複チェック(ユーザー編集時)
	  * @param connection
	  * @param user
	  * @param loginId
	  * @return
	  */
	public static boolean isDuplicatonUser(Connection connection, int userId, String loginId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");
			sql.append(" WHERE login_id = ?");
			sql.append(" AND id != ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);
			ps.setInt(2, userId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * ユーザーの削除
	  * @param userId
	  */
	public static void delete(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM users WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, userId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 店長の重複チェック(ユーザー編集時)
	  * @param connection
	  * @param id
	  * @param branchId
	  * @return
	  */
	public static boolean isDuplicationStoreManager(Connection connection, int id, int branchId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");
			sql.append(" WHERE id != ?");
			sql.append(" AND branch_id = ?");
			sql.append(" AND department_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			ps.setInt(2, branchId);
			ps.setInt(3, STORE_MANAGER);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 店長の重複チェック(ユーザー登録時)
	  * @param connection
	  * @param branchId
	  * @return
	  */
	public static boolean isDuplicationStoreManager(Connection connection, int branchId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");
			sql.append(" WHERE branch_id = ?");
			sql.append(" AND department_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, branchId);
			ps.setInt(2, STORE_MANAGER);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
