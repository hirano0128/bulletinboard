package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;
import exception.SQLRuntimeException;
import utils.CipherUtil;

public class LoginService {

	/**
	  * ログイン
	  * @param loginId
	  * @param password
	  * @return
	  */
	public User login(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();	// DBへの接続

			String encPassword = CipherUtil.encrypt(password);	// パスワードの暗号化

			// ユーザーが有効なときのみログインできるようにする
			User user = UserDao.getLoginUser(connection, loginId, encPassword);	// ユーザーの取得

			commit(connection);

			return user;
		} catch (SQLRuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
