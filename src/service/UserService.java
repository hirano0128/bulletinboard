package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	/**
	  * ユーザーの登録
	  * @param user
	  */
	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			// パスワードの暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao.insert(connection, user);	// ユーザーの追加

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * すべてのユーザーを取得
	  * @return
	  */
	public List<User> getUserAll() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<User> userList = UserDao.getUserAll(connection);

			commit(connection);

			return userList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ユーザー情報の更新
	  * @param user
	  */
	public void editUser(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			// パスワードの暗号化
			if (user.getPassword() != null) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao.updateUser(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ユーザー情報の更新(管理者権限あり)
	  * @param user
	  */
	public void editAuthorityUser(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			// パスワードの暗号化
			if (user.getPassword() != null) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao.updateAuthorityUser(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * パスワードの変更
	  * @param loginUser
	  * @param password
	  */
	public void updatePassword(User loginUser, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			// パスワードの暗号化
			String encPassword = CipherUtil.encrypt(password);

			UserDao.updatePassword(connection, loginUser, encPassword);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * IDからユーザーを取得
	  * @param userId
	  * @return
	  */
	public User getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			User user = UserDao.getUser(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ログインIDからユーザーを取得
	  * @param userId
	  * @return
	  */
	public User getUser(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();

			User user = UserDao.getUser(connection, loginId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ユーザーの停止・復活
	  * @param user
	  */
	public void isStopped(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao.isStopped(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ユーザー(ログインID)の重複チェック(ユーザー登録時)
	  * @param loginId
	  * @return
	  */
	public boolean isDuplicationUser(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();

			boolean is = UserDao.isDuplicatonUser(connection, loginId);

			commit(connection);

			return is;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ユーザー(ログインID)の重複チェック(ユーザー編集時)
	  * @param loginId
	  * @return
	  */
	public boolean isDuplicationUser(int userId, String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();

			boolean is = UserDao.isDuplicatonUser(connection, userId, loginId);

			commit(connection);

			return is;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * ユーザーの削除
	  * @param userId
	  */
	public void deleteUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao.delete(connection, userId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 店長の重複チェック(ユーザー編集時)
	  * @param id
	  * @param branchId
	  * @return
	  */
	public boolean isDuplicationStoreManager(int id, int branchId) {

		Connection connection = null;
		try {
			connection = getConnection();

			boolean is = UserDao.isDuplicationStoreManager(connection, id, branchId);

			commit(connection);

			return is;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 店長の重複チェック(ユーザー登録時)
	  * @param branchId
	  * @return
	  */
	public boolean isDuplicationStoreManager(int branchId) {

		Connection connection = null;
		try {
			connection = getConnection();

			boolean is = UserDao.isDuplicationStoreManager(connection, branchId);

			commit(connection);

			return is;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}



}
