package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import beans.Post;
import dao.PostDao;

public class PostService {
	/**
	  * 投稿の登録
	  * @param post
	  */
	public void register(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao.insert(connection, post);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * すべての投稿を取得
	  * @return
	  */
	public List<Post> getPostAll() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<Post> postList = PostDao.getPostAll(connection);

			commit(connection);

			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * すべてのカテゴリーを取得
	  * @return
	  */
	public List<String> getCategoryAll() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<String> categoryList = PostDao.getCategoryAll(connection);

			commit(connection);

			return categoryList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 投稿の削除
	  * @param postId
	  */
	public void deletePost(int postId) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao.delete(connection, postId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 投稿の検索
	  * @param category
	  * @param startDate
	  * @param endDate
	  * @param sortType
	  * @return
	  */
	public List<Post> searchPost(String category, String firstDate, String lastDate, int sortType) {

		Connection connection = null;
		try {
			connection = getConnection();

			List<Post> postList = PostDao.searchPost(connection, category, firstDate, lastDate, sortType);

			commit(connection);

			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 最古の投稿日時を取得
	  * @return
	  */
	public String getFirstDate() throws ParseException {

		Connection connection = null;
		try {
			connection = getConnection();

			String firstDate = PostDao.getFirstDate(connection);

			commit(connection);

			return firstDate + " 00:00:00";
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 最新の投稿日時を取得
	  * @return
	  */
	public String getLastDate() {

		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("yyyy-MM-dd");

		Date date = new Date();
		String ret = sdf.format(date);

		return ret + " 23:59:59";

		/*Connection connection = null;
		try {
			connection = getConnection();

			String lastDate = PostDao.getLastDate(connection);

			commit(connection);

			return lastDate;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}*/
	}

}
