package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;

public class DepartmentService {
	/**
	  * すべての部署・役職を取得
	  * @return
	  */
	public List<Department> getDepartmentAll() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<Department> ret = DepartmentDao.getDepartmentAll(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 部署・役職の名前から部署・役職を取得
	  * @param departmentName
	  * @return
	  */
	public Department getDepartment(String departmentName) {

		Connection connection = null;
		try {
			connection = getConnection();

			Department ret = DepartmentDao.getDepartment(connection, departmentName);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 部署・役職IDから部署・役職を取得
	  * @param departmentName
	  * @return
	  */
	public Department getDepartment(int departmentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			Department ret = DepartmentDao.getDepartment(connection, departmentId);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 部署・役職の名前から部署・役職IDを取得
	  * @param departmentName
	  * @return
	  */
	public int getDepartmentId(String departmentName) {

		Connection connection = null;
		try {
			connection = getConnection();

			int ret = DepartmentDao.getDepartmentId(connection, departmentName);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 部署・役職IDから部署・役職の名前を取得
	  * @param departmentName
	  * @return
	  */
	public String getDepartmentName(int departmentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			String ret = DepartmentDao.getDepartmentName(connection, departmentId);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 部署・役職が存在することを確かめる
	  * @param branchId
	  * @return
	  */
	public boolean isExists(int departmentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			if (DepartmentDao.isExists(connection, departmentId)) {
				return true;
			} else {
				return false;
			}

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
