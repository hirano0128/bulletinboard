package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exception.SQLRuntimeException;

/**
  * DB(コネクション関係)のユーティリティ

  */
public class DBUtil {
	// MySQL Connection Info
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/bulletin_board";
	private static final String USER = "root";
	private static final String PASSWORD = "4Merry@Nightmear";

	// Static Initializer(静的初期化)
	static {
		try {
			Class.forName(DRIVER);	// ドライバの読み込み
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	  * DBへのコネクションの取得
	  * @return connection
	  */
	public static Connection getConnection() {

		try {
			Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

			connection.setAutoCommit(false);

			return connection;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	  * コミット
	  * @param connection
	  */
	public static void commit(Connection connection) {

		try {
			connection.commit();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * ロールバック
	 * @param connection
	 */
	public static void rollback(Connection connection) {

		if (connection == null) {
			return;
		}

		try {
			connection.rollback();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}
