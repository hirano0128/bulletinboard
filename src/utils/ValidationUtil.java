package utils;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
  * バリデーションユーティリティ

  */
public class ValidationUtil {

	public static boolean isValidEmpty(String str) {
		return StringUtils.isEmpty(str);
	}

	public static boolean isValidBlank(String str) {
		return StringUtils.isBlank(str);
	}

	public static boolean isValidFormat(String regex, String str) {
		return str.matches(regex);
	}

	public static boolean isValidRange(int min, int max, String str) {
		return str.length() >= min && str.length() <= max;
	}

	public static boolean isValidEquals(String str1, String str2) {
		return StringUtils.equals(str1, str2);
	}

	public static boolean isNotNull(String str) {
		return (str != null) ? true : false;
	}

	public static boolean isNull(String str) {
		return (str == null) ? true : false;
	}

	public static boolean isNullMapValue(Map<String, String> map) {
		for (String key : map.keySet()) {
			if (isNotNull(map.get(key))) {
				return false;
			}
		}
		return true;
	}
}
