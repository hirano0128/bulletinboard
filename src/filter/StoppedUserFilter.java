package filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

/**
 * ユーザー停止フィルター
 * 停止されたユーザーをログイン不可にする

 */
@WebFilter(filterName = "stoppedfilter", urlPatterns = { "/*" })
public class StoppedUserFilter implements Filter {

	private static final int USER_STOP = 0;	// 停止状態

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		// ログインユーザーを取得
		User loginUser = (User) session.getAttribute("loginUser");

		Map<String, String> messages = new HashMap<String, String>();

		if (loginUser != null) {
			// ログインユーザー情報の取得
			User user = new UserService().getUser(loginUser.getId());

			if (user.getIsStopped() == USER_STOP) {
				messages.put("stoppedCheck", "そのユーザーは停止されています");

				session.setAttribute("errorMessages", messages);
				session.removeAttribute("loginUser");

				((HttpServletResponse) response).sendRedirect("login");

				return;
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}

}
