 
/* ユーザーテーブル */
CREATE TABLE users(
    id				INT 			AUTO_INCREMENT PRIMARY KEY
  , login_id 		VARCHAR(20)		UNIQUE NOT NULL
  , password		VARCHAR(255)	NOT NULL
  , name			VARCHAR(10)		NOT NULL
  , is_stopped		INT 			NOT NULL
  , branch_id		INT				NOT NULL
  , department_id	INT				NOT NULL
  , created_at		TIMESTAMP		NOT NULL
  , update_at		TIMESTAMP		NOT NULL
);

/* 支店テーブル */
CREATE TABLE branches(
	id		INT				AUTO_INCREMENT PRIMARY KEY
  , name	VARCHAR(20)		UNIQUE NOT NULL
);

/* 部署テーブル */
CREATE TABLE departments(
	id		INT				AUTO_INCREMENT PRIMARY KEY
  , name	VARCHAR(20)		UNIQUE NOT NULL
);

/* 投稿テーブル */
CREATE TABLE posts(
	id				INT			AUTO_INCREMENT PRIMARY KEY
  , title			VARCHAR(50)	NOT NULL
  , text			TEXT		NOT NULL
  , category		VARCHAR(10)	NOT NULL
  , created_at		TIMESTAMP	NOT NULL
  , update_at		TIMESTAMP	NOT NULL
  , user_id			INT			NOT NULL
  , branch_id		INT			NOT NULL
  , department_id	INt			NOT NULL
);

/* コメントテーブル */
CREATE TABLE comments(
	id				INT			AUTO_INCREMENT PRIMARY KEY
  , text			TEXT		NOT NULL
  , created_at		TIMESTAMP	NOT NULL
  , update_at		TIMESTAMP	NOT NULL
  , user_id			INT			NOT NULL
  , post_id			INT			NOT NULL
  , branch_id		INT			NOT NULL
  , department_id	INT			NOT NULL
);
