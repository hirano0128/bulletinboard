<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ユーザー管理</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/blitzer/jquery-ui.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ja.min.js"></script>

	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/bbs.js"></script>
</head>
<body>

<header>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a href="./" class="navbar-brand">掲示板</a>
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
        		</button>
			</div>

      		<div class="navbar-collapse collapse" id="navbar-main">
      			<ul class="nav navbar-nav">
      				<li><a href="./">ホーム</a></li>
          			<li>
          				<c:if test="${ loginUser.branchId == 1 && loginUser.departmentId == 1 }">
							<a href="signup">ユーザー新規登録</a>
						</c:if>
					</li>

          			<li class="dropdown active">
            			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            				<c:out value="${loginUser.name}" /> <span class="caret"></span>
            			</a>
            			<ul class="dropdown-menu" role="menu">
              				<li><a href="logout">ログアウト</a></li>
            			</ul>
          			</li>
        		</ul>
      		</div>
    	</div>
  	</div>
</header>

<div class="container">
	<table class="center table table-bordered table-hover ">
		<thead>
			<tr>
				<th class="center">ログインID</th>
				<th class="center">名称</th>
				<th class="center">支店</th>
				<th class="center">部署・役職</th>
				<th class="center"></th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${userList}" var="user">
				<tr>
					<td><c:out value="${user.loginId}" /></td>

					<td><c:out value="${user.name}" /></td>

					<c:forEach items="${branchList}" var="branch">
						<c:if test="${ branch.id == user.branchId }">
							<td><c:out value="${branch.name}" /></td>
						</c:if>
					</c:forEach>

					<c:forEach items="${departmentList}" var="department">
						<c:if test="${ department.id == user.departmentId }">
							<td><c:out value="${department.name}" /></td>
						</c:if>
					</c:forEach>

					<td class="col-lg-3">
						<form action="editUser" method="get" class="col-lg-2 col-lg-offset-1">
							<button class="btn btn-success" type="submit" name="editUserId" value="${user.id}">編集</button>
						</form>
						<c:if test="${ user.id != loginUser.id }">
							<form action="adminUser" method="post" class="col-lg-2 col-lg-offset-1">
								<c:if test="${ user.isStopped == 1 }">
									<button class="btn btn-warning" type="submit" name="stopUser" value="${user.id}" onClick="return stopDialog()">停止</button>
								</c:if>

								<c:if test="${ user.isStopped == 0 }">
									<button class="btn btn-warning" type="submit" name="stopUser" value="${user.id}" onClick="return activeDialog()">復活</button>
								</c:if>
							</form>

							<form action="deleteUser" method="post" class="col-lg-2 col-lg-offset-1">
								<button class="btn btn-danger" type="submit" name="deleteUser" value="${user.id}" onClick="return deleteDialog()">削除</button>
							</form>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<div class="copyright">Copyright(c) Takumi Urushibata</div>
</body>
</html>