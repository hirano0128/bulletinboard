<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ユーザー編集</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/blitzer/jquery-ui.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ja.min.js"></script>

	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/bbs.js"></script>
</head>
<body>

<header>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a href="./" class="navbar-brand">掲示板</a>
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
        		</button>
			</div>

      		<div class="navbar-collapse collapse" id="navbar-main">
      			<ul class="nav navbar-nav">
      				<li><a href="./">ホーム</a></li>
          			<li>
          				<c:if test="${ loginUser.branchId == 1 && loginUser.departmentId == 1 }">
							<a href="adminUser">ユーザー管理</a>
						</c:if>
					</li>

          			<li class="dropdown active">
            			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            				<c:out value="${loginUser.name}" /> <span class="caret"></span>
            			</a>
            			<ul class="dropdown-menu" role="menu">
              				<li><a href="logout">ログアウト</a></li>
            			</ul>
          			</li>
        		</ul>
      		</div>
    	</div>
  	</div>
</header>

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="well bs-component">
	         	<form action="editUser" method="post" autocomplete="off">
	            	<fieldset>
	              		<legend>ユーザー情報編集</legend>

	              		<div class="form-group col-lg-8 col-lg-offset-2">
	               			<label for="loginId">ログインID(半角英数字6～20文字以内)</label>
	                  		<input class="form-control" type="text" name="loginId" placeholder="ログインIDを変更" maxlength="20" id="loginId" value="${user.loginId}" />
	                		<div class="errorMessages">
								<c:out value="${errorMessages.loginId}"/>
							</div>
	                	</div>

	             		<div class="form-group col-lg-8 col-lg-offset-2">
		              		<label for="password">パスワード(半角文字6～20文字以内)</label>
		                  	<input class="form-control" name="password" placeholder="変更しない場合は何も入力しないでください" maxlength="20" type="password" id="password" value="${user.password}" />
		                  	<div class="errorMessages">
		                  		<c:out value="${errorMessages.password}"/>
		                  	</div>
	                	</div>

	                	<div class="form-group col-lg-8 col-lg-offset-2">
		              		<label for="confirmation">パスワードを再入力</label>
		                  	<input class="form-control" name="confirmation" placeholder="パスワードを再入力" maxlength="20" type="password" id="confirmation" value="${confirmation}" />
		                  	<div class="errorMessages">
		                  		<c:out value="${errorMessages.confirmation}"/>
		                  	</div>
	                	</div>

	                	<div class="form-group col-lg-8 col-lg-offset-2">
		              		<label for="name">名称(10文字以内)</label>
		                  	<input class="form-control" name="name" placeholder="名称を入力" maxlength="10" type="text" id="name" value="${user.name}" />
		                  	<div class="errorMessages">
		                  		<c:out value="${errorMessages.name}"/>
		                  	</div>
	                	</div>

						<c:if test="${ user.id != loginUser.id }">
							<div class="form-group col-lg-8 col-lg-offset-2">
	                			<label for="branch">支店</label>
	                   			<select class="form-control" name="branches" id="branch" size="1">
									<c:forEach items="${branchList}" var="branch">
										<option value="${branch.id}" <c:if test="${ branch.id == user.branchId }">selected</c:if> >
											<c:out value="${branch.name}"/>
										</option>
									</c:forEach>
								</select>
								<div class="errorMessages">
									<c:out value="${errorMessages.branch}"/>
								</div>
	                  		</div>

		                	<div class="form-group col-lg-8 col-lg-offset-2">
	                			<label for="department">部署・役職</label>
	                   			<select class="form-control" name="departments" id="deaprtment" size="1">
									<c:forEach items="${departmentList}" var="department">
										<option value="${department.id}" <c:if test="${ department.id == user.departmentId }">selected</c:if> >
											<c:out value="${department.name}"/>
										</option>
									</c:forEach>
								</select>
								<div class="errorMessages">
									<c:out value="${errorMessages.department}"/>
								</div>
	                  		</div>
                  		</c:if>

			            <div class="form-group">
			            	<div class="col-lg-10 col-lg-offset-2">
			            		<button type="submit" class="btn btn-primary" name="userId" value="${user.id}">変更</button>
			            	</div>
			            </div>
	            	</fieldset>
	         	 </form>
	        </div>
		</div>
	</div>

	<c:remove var="errorMessages" scope="session" />
	<div class="copyright">Copyright(c) Takumi Urushibata</div>
</div>
s
</body>
</html>