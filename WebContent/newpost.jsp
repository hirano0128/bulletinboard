<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>新規投稿</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/blitzer/jquery-ui.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ja.min.js"></script>

	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/bbs.js"></script>
</head>
<body>

<script>

</script>

<header>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">掲示板</a>
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
        		</button>
			</div>

      		<div class="navbar-collapse collapse" id="navbar-main">
      			<ul class="nav navbar-nav">
      				<li><a href="./">ホーム</a></li>

          			<li class="dropdown active">
            			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            				<c:out value="${loginUser.name}" /> <span class="caret"></span>
            			</a>
            			<ul class="dropdown-menu" role="menu">
              				<li><a href="logout">ログアウト</a></li>
            			</ul>
          			</li>
        		</ul>
      		</div>
    	</div>
  	</div>
</header>

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="well bs-component">
	         	<form action="newPost" method="post" autocomplete="off">
	            	<fieldset>
	              		<legend>ユーザー新規登録</legend>

	              		<div class="form-group col-lg-8 col-lg-offset-2">
	              			<label for="title">件名(30文字以内)</label>
							<input class="form-control" name="title" placeholder="件名を入力" maxlength="30" id="title" value="${post.title}" />
							<div class="errorMessages">
								<c:out value="${errorMessages.title}"/>
							</div>
	                	</div>

	                	<div class="form-group col-lg-8 col-lg-offset-2">
	                		<label for="category">カテゴリー(10文字以内)</label>
	                		<input class="form-control" name="category" placeholder="カテゴリーを入力" maxlength="10" id="category" value="${post.category}" />
							<div class="errorMessages">
								<c:out value="${errorMessages.category}"/>
							</div>
	                	</div>

						<%-- <div class="form-group col-lg-8 col-lg-offset-2">
                			<label for="category">カテゴリー</label>
							<select class="form-control" name="categories" id="category" size="1">
								<option value="">カテゴリーを選択して下さい</option>
								<c:forEach items="${categoryList}" var="category">
									<c:if test="${ post.category == category }" var="isSelected"/>
									<option value="${category}" <c:if test="${isSelected}">selected</c:if> >
										<c:out value="${category}" />
									</option>
								</c:forEach>
							</select>
							<div class="errorMessages">
								<c:out value="${errorMessages.category}"/>
							</div>
                  		</div> --%>

	                	<div class="form-group col-lg-8 col-lg-offset-2">
                			<label for="text">本文(1000文字以内)</label>
							<textarea class="form-control" name="text" rows="10" id="text" placeholder="本文を入力" maxlength="1000"><c:out value="${post.text}" /></textarea>
							<div class="errorMessages">
								<c:out value="${errorMessages.text}"/>
							</div>
                  		</div>

			            <div class="form-group">
			            	<div class="col-lg-10 col-lg-offset-2">
			            		<button type="submit" class="btn btn-primary">投稿</button>
			            	</div>
			            </div>
	            	</fieldset>
	         	 </form>
	        </div>
		</div>
	</div>

	<c:remove var="errorMessages" scope="session" />
	<div class="copyright">Copyright(c) Takumi Urushibata</div>
</div>

</body>
</html>