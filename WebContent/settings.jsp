<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>パスワード変更</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/blitzer/jquery-ui.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ja.min.js"></script>

	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/bbs.js"></script>
</head>
<body>

<header>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a href="./" class="navbar-brand">掲示板</a>
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
        		</button>
			</div>

      		<div class="navbar-collapse collapse" id="navbar-main">
      			<ul class="nav navbar-nav">
      				<li><a href="./">ホーム</a></li>

          			<li class="dropdown active">
            			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            				<c:out value="${loginUser.name}" /> <span class="caret"></span>
            			</a>
            			<ul class="dropdown-menu" role="menu">
              				<li><a href="logout">ログアウト</a></li>
            			</ul>
          			</li>
        		</ul>
      		</div>
    	</div>
  	</div>
</header>

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="well bs-component">
	         	<form action="settings" method="post" autocomplete="off">
	            	<fieldset>
	              		<legend>パスワード変更</legend>

	             		<div class="form-group col-lg-8 col-lg-offset-2">
		              		<label for="oldPassword">現在のパスワード</label>
							<input class="form-control" name="oldPassword" placeholder="現在のパスワードを入力" type="password" maxlength="20" id ="oldPassword" value="${oldPassword}"/>
							<div class="errorMessages">
								<c:out value="${errorMessages.oldPassword}"/>
							</div>
	                	</div>

						<div class="form-group col-lg-8 col-lg-offset-2">
		              		<label for="password">新しいパスワード(半角文字6～20文字以内)</label>
							<input class="form-control" name="password" placeholder="新しいパスワードを入力" type="password" maxlength="20" id="password" value="${password}"/>
							<div class="errorMessages">
								<c:out value="${errorMessages.password}"/>
							</div>
	                	</div>

	                	<div class="form-group col-lg-8 col-lg-offset-2">
		              		<label for="confirmation">パスワードを再入力</label>
		                  	<input class="form-control" name="confirmation" placeholder="パスワードを再入力" maxlength="20" type="password" id="confirmation" value="${confirmation}" />
		                  	<div class="errorMessages">
		                  		<c:out value="${errorMessages.confirmation}"/>
		                  	</div>
	                	</div>

			            <div class="form-group">
			            	<div class="col-lg-10 col-lg-offset-2">
			            		<button type="submit" class="btn btn-primary">変更</button>
			            	</div>
			            </div>
	            	</fieldset>
	         	 </form>
	        </div>
		</div>
	</div>

	<c:remove var="errorMessages" scope="session" />
	<div class="copyright">Copyright(c) Takumi Urushibata</div>
</div>

</body>
</html>