<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ホーム</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/blitzer/jquery-ui.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ja.min.js"></script>

	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/bbs.js"></script>
</head>
<body>

<script>
$(function() {
	$(".datepicker").datepicker({
	    language: 'ja',
	    format: "yyyy-mm-dd",
	    autoclose: true,
	    orientation: "bottom auto"
	});

	$("#firstDate").datepicker({
		maxDate : "0y"
	});

	$("#firstDate").change(function() {
		$("#lastDate").datepicker("option", "minDate", $("#firstDate").val());
	});

	$("#lastDate").change(function() {
		$("#firstDate").datepicker("option", "maxDate", $("#lastDate").val());
	});

	$("#lastDate").datepicker({
		maxDate : "0y"
	});
});
</script>

<header>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a href="./" class="navbar-brand">掲示板</a>
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
          			<span class="icon-bar"></span>
        		</button>
			</div>

      		<div class="navbar-collapse collapse" id="navbar-main">
      			<ul class="nav navbar-nav">
      				<li><a href="./">ホーム</a></li>
      				<li><a href="newPost">新規投稿</a></li>
          			<li>
          				<c:if test="${ loginUser.branchId == 1 && loginUser.departmentId == 1 }">
							<a href="adminUser">ユーザー管理</a>
						</c:if>
					</li>

          			<li class="dropdown active">
            			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            				<c:out value="${loginUser.name}" /> <span class="caret"></span>
            			</a>
            			<ul class="dropdown-menu" role="menu">
            				<li><a href="settings">パスワード変更</a></li>
              				<li><a href="logout">ログアウト</a></li>
            			</ul>
          			</li>
        		</ul>
      		</div>
    	</div>
  	</div>
</header>

<div class="container">
	<c:if test="${ not empty errorMessages.comentText || not empty errorMessages.jspCheck || not empty errorMessages.authorityCheck }">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				<div class="bs-component">
					<div class="alert alert-dismissible alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<c:out value="${errorMessages.jspCheck}"/>
						<c:out value="${errorMessages.authorityCheck}"/>
						<c:out value="${errorMessages.commentText}"/>
						<c:remove var="errorMessages" scope="session"/>
					</div>
				</div>
			</div>
		</div>
	</c:if>

	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="well bs-component">
				<form action="./" method="get">
					<fieldset>
	              		<legend>投稿検索</legend>

						<div class="form-group col-lg-8 col-lg-offset-2">
                			<label for="categories">カテゴリー</label>
                   			<select  class="form-control" name="categories" id="categories">
								<option value="">カテゴリーを選択して下さい</option>
								<c:forEach items="${categoryList}" var="category">
									<c:if test="${ selectedCategory == category }" var="isSelected"/>
									<option value="${category}" <c:if test="${isSelected}">selected</c:if> >
										<c:out value="${category}"/>
									</option>
								</c:forEach>
							</select>
                  		</div>

                  		<div class="date form-group col-lg-8 col-lg-offset-2 date">
							<label for="firstDate">日付</label>
								<div class="input-group input-daterange">
									<div class="input-group-addon">from</div>
								    <input type="text" class="form-control datepicker" id="firstDate" name="firstDate" autocomplete="off" min="${minDate}" max="${maxDate}" value="${firstDate}">
								    <div class="input-group-addon">to</div>
								    <input type="text" class="form-control datepicker" id="lastDate" name="lastDate" autocomplete="off" min="${minDate}" max="${maxDate}" value="${lastDate}">
								</div>
							<%-- <div class="form-inline">
							<input type="text" class="form-control datepicker" id="firstDate" name="firstDate" placeholder="開始日" autocomplete="off" min="${minDate}" max="${maxDate}" value="${firstDate}"/>
							～
							<input type="text" class="form-control datepicker" id="lastDate" name="lastDate" placeholder="終了日" autocomplete="off" min="${minDate}" max="${maxDate}" value="${lastDate}"/>
							</div> --%>
						</div>

						<div class="form-group col-lg-8 col-lg-offset-2 form-inline">
                  			<div class="radio">
                    			<label>
                    				<input type="radio" name="sortType" value="1" <c:if test="${sortType == '1'}">checked</c:if> >
                     				新しい順
                   				</label>
                  			</div>

	                		<div class="radio">
	                    		<label>
	                      			<input type="radio" name="sortType" value="0" <c:if test="${sortType == '0'}">checked</c:if> >
	                     			古い順
	                    		</label>
	                  		</div>
              			</div>

              			<div class="form-group">
	               	 		<div class="col-lg-10 col-lg-offset-2">
	                  			<button type="submit" class="btn btn-primary">検索</button>
	                  			<a href="./"><input class="btn btn-default" type="button" value="リセット"/></a>
	                		</div>
	              		</div>
	              	</fieldset>
				</form>
			</div>
		</div>
	</div>

	<c:if test="${ not empty postList }">
		<c:forEach items="${postList}" var="post">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="bs-component">
						<div class="panel panel-info">
							<div class="panel-heading">
								<div class="panel-title">
									<c:forEach items="${userList}" var="user">
										<c:if test="${ user.id == post.userId }">
											<h4><c:out value="${user.name}" /> さんの投稿</h4>
										</c:if>
									</c:forEach>
									件名 : <c:out value="${post.title}"/> <br />
		              				カテゴリー : <c:out value="${post.category}"/> <br />
		              				投稿日時：<fmt:formatDate value="${post.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" /> <br />
            					</div>

            					<div class="panel-body">
            						<div class="panel panel-default">
            							<div class="panel-body">
            								<p class="text"><c:out value="${post.text}"/></p>
            							</div>
            						</div>

		              				<c:if test="${ post.userId == loginUser.id
												|| ( loginUser.branchId == 1 && loginUser.departmentId == 2 )
												|| (post.branchId == loginUser.branchId && loginUser.departmentId == 3) }">
										<form action="deletePost" method="post" onClick="return postDialog()">
											<button class="btn btn-danger" type="submit" name="deletePost" id="deletePost" value="${post.id}">投稿削除</button>
										</form>
									</c:if>
		    			        </div>
            				</div>

            				<div class="commentBlock"><button class="form-control btn btn-default">コメント表示</button></div>
							<div class="commentNone"><button class="form-control btn btn-default">コメント非表示</button></div>
            			</div>

						<!-- <div class="comment"> -->
							<!-- <div class="panel-heading">
						        <div class="panel-title">この投稿へのコメント</div>
							</div> -->

							<c:forEach items="${commentList}" var="comment">
								<c:if test="${ post.id == comment.postId }">
									<div class="panel panel-success">
						          	<div class="panel-body">
						            	<c:forEach items="${userList}" var="user">
											<c:if test="${ user.id == comment.userId }">
												<c:out value="${user.name}" /> : <br />
											</c:if>
										</c:forEach>
										<c:out value="${comment.text}" /> <br />
										<fmt:formatDate value="${comment.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" />

							         	<c:if test="${ comment.userId == loginUser.id
													|| ( loginUser.branchId == 1 && loginUser.departmentId == 2 )
													|| (comment.branchId == loginUser.branchId && loginUser.departmentId == 3) }">
											<form action="deleteComment" method="post" onClick="return commentDialog()">
												 <button class="btn btn-danger" type="submit" name="deleteComment" value="${comment.id}">コメント削除</button>
											</form>
										</c:if>
									</div>
									</div>
								</c:if>
							</c:forEach>
						<!-- </div> -->

            			<!-- コメントの入力 -->
						<form action="newComment" method="post" autocomplete="off">
							<div class="form-group">
								<label for="commentText">コメント(500文字以内)</label>
								<textarea class="form-control" name="commentText" rows="2" id="commentText" placeholder="コメントを入力" maxlength="500"><c:out value="${commentText}" /></textarea>
								<button class="btn btn-primary" type="submit" name="postId" value="${post.id}">コメントする</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</c:forEach>
	</c:if>
</div>

</body>
</html>