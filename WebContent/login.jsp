<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ログイン</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/blitzer/jquery-ui.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ja.min.js"></script>

	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/bbs.js"></script>
</head>
<body>

<header>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<h3>掲示板</h3>
			</div>
		</div>
	</div>
</header>

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<div class="well bs-component">
	         	<form action="login" method="post" autocomplete="off">
	            	<fieldset>
	              		<legend>ログイン</legend>

						<c:if test="${ not empty errorMessages.loginCheck || not empty errorMessages.stoppedCheck }">
		              		<div class="form-group col-lg-8 col-lg-offset-2">
			              		<div class="alert alert-dismissible alert-danger">
			              			<button type="button" class="close" data-dismiss="alert">&times;</button>
									<c:out value="${errorMessages.loginCheck}"/>
									<c:out value="${errorMessages.stoppedCheck}"/>
									<c:remove var="errorMessages" scope="session"/>
								</div>
		              		</div>
	              		</c:if>

	              		<div class="form-group col-lg-8 col-lg-offset-2">
	               			<label for="loginId">ログインID</label>
	                  		<input type="text" class="form-control" id="loginId" name="loginId" value="${loginId}">
	                	</div>

	             		 <div class="form-group col-lg-8 col-lg-offset-2">
		              		 <label for="password">パスワード</label>
		                  	 <input type="password" class="form-control" id="password" name="password" value="${password}">
	                	 </div>

			             <div class="form-group">
			             	<div class="col-lg-10 col-lg-offset-2">
			             		<button type="submit" class="btn btn-primary">ログイン</button>
			             	</div>
			             </div>
	            	</fieldset>
	         	 </form>
	        </div>
		</div>
	</div>

	<div class="copyright">Copyright(c) Takumi Urushibata</div>
</div>
</body>
</html>